package extractor

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"sync"
)

// Resource data returned from em7 rest api
type Resource struct {
	SearchSpec    json.RawMessage        `json:"searchspec"`
	TotalMatched  int                    `json:"total_matched"`
	TotalReturned int                    `json:"total_returned"`
	ResultSet     map[string]interface{} `json:"result_set"`
}

// ResourceExtractor retrieves data from em7 rest api
type ResourceExtractor struct {
	Name  string
	Paths map[string]interface{}

	extractor *Em7Extractor
}

// Extract recursively pulls data from the em7 rest api based on paths
func (r *ResourceExtractor) Extract(out chan Object) {
	wg := sync.WaitGroup{}
	page := 0

	// grab the first page to determine how many subsequent pages need to be
	// grabbed
	res, err := r.getResourcePage(page)
	if err != nil {
		info.Printf("resource call failed on %s: %v", r.Name, err)
		return
	}
	info.Printf("extracting %s: %d", r.Name, res.TotalMatched)

	// grab nested fields for initial page
	wg.Add(1)
	go func() {
		defer wg.Done()
		out <- r.getNestedFields(res)
	}()

	// only get first page when running tests
	if r.extractor.test {
		wg.Wait()
		close(out)
		return
	}

	// grab the rest of the pages and any nested information
	for rem := res.TotalMatched - (page+1)*PAGESIZE; rem > 0; rem = rem - PAGESIZE {
		wg.Add(1)
		page++
		go func(p int) {
			defer wg.Done()

			res, err = r.getResourcePage(p)
			if err != nil {
				info.Printf("resource page failed %d: %v", p, err)
				return
			}
			out <- r.getNestedFields(res)
		}(page)
	}
	wg.Wait()
}

// getNestedFields concurrently populated the nested data fields based on the
// resource paths
func (r *ResourceExtractor) getNestedFields(res *Resource) Object {
	wg := &sync.WaitGroup{}
	data := make([]map[string]interface{}, len(res.ResultSet))
	index := 0

	for key, val := range res.ResultSet {
		wg.Add(1)
		go func(v map[string]interface{}, k string, i int) {
			defer wg.Done()

			v["id"] = k
			if r.Name == "device_group" {
				r.getExpandedDevices(v)
			} else {
				r.getNested(v)
			}
			data[i] = v
		}(val.(map[string]interface{}), key, index)
		index++
	}

	wg.Wait()
	return Object{Name: r.Name, Data: data}
}

// getExpandedDevices grabs the expanded devices from the em7 api for
// device groups.
func (r *ResourceExtractor) getExpandedDevices(v map[string]interface{}) {
	path := fmt.Sprintf("%s/expanded_devices", v["id"])
	info.Printf("Requesting NESTED info %s", path)
	respc, errc := get(r.extractor, path, retry)
	resp := <-respc
	err := <-errc
	if err != nil {
		info.Printf("could not get device group expanded devices %s: %v", v["id"], err)
	} else {

		if resp == nil {
			info.Printf("nested decode json failed: RESPONSE OBJECT NIL")
			return
		}
		buf := new(bytes.Buffer)
		buf.ReadFrom(resp.Body)
		s := buf.String()

		var val interface{}
		if err := json.NewDecoder(strings.NewReader(s)).Decode(&val); err != nil {
			info.Printf("nested decode json failed: %v, %s", err, s)
			return
		}
		v["devices"] = val
	}
}

func (r *ResourceExtractor) getConfigData(v map[string]interface{}) {
	path := fmt.Sprintf("%s/config_data", v["id"])
	respc, errc := get(r.extractor, path, retry)
	resp := <-respc
	err := <-errc
	if err != nil {
		info.Printf("could not get device config data %s: %v", v["id"], err)
	} else {

		if resp == nil {
			info.Printf("nested decode json failed: RESPONSE OBJECT NIL")
			return
		}
		buf := new(bytes.Buffer)
		buf.ReadFrom(resp.Body)
		s := buf.String()

		var val map[string]interface{}
		if err := json.NewDecoder(strings.NewReader(s)).Decode(&val); err != nil {
			info.Printf("nested decode json failed: %v, %s", err, s)
			return
		}

		r.getConfigDataData(val)
		v["config_data"] = val
	}
}

func (r *ResourceExtractor) getConfigDataData(v map[string]interface{}) {
	var wg sync.WaitGroup
	resultSet, ok := v["result_set"].(map[string]interface{})
	if !ok {
		// info.Println("could not get result set from config data")
		return
	}

	wg.Add(len(resultSet))
	for _, data := range resultSet {
		go func(d map[string]interface{}) {
			defer wg.Done()
			path := d["data"].(map[string]interface{})["URI"].(string)
			respc, errc := get(r.extractor, path, retry)
			resp := <-respc
			err := <-errc
			if err != nil {
				info.Println("error durring config data data call:", err)
				return
			}

			var val interface{}
			if err := json.NewDecoder(resp.Body).Decode(&val); err != nil {
				info.Println("config_data data decode json failed:", err)
				return
			}
			d["data"] = val
		}(data.(map[string]interface{}))
	}
	wg.Wait()
}

// getNested determines the type of nested field based on resource, makes
// request to em7 api, and populated results. Currently on supports subresources.
func (r *ResourceExtractor) getNested(resource map[string]interface{}) {
	for p := range r.Paths {
		switch s := resource[p].(type) {
		case map[string]interface{}:
			if p == "config_data" {
				// handle device.config_data.data."capacity"
				r.getConfigData(resource)
				break
			}
			path := s["URI"].(string)
			respc, errc := get(r.extractor, path, retry)
			resp := <-respc
			if err := <-errc; err != nil {
				info.Println("get nested failed:", err)
				return
			}

			if resp == nil {
				info.Printf("nested decode json failed: RESPONSE OBJECT NIL")
				return
			}
			buf := new(bytes.Buffer)
			buf.ReadFrom(resp.Body)
			st := buf.String()

			var val interface{}
			if err := json.NewDecoder(strings.NewReader(st)).Decode(&val); err != nil {
				info.Printf("nested decode json failed: %v, %s", err, st)
				return
			}
			resource[p] = val
		}
	}
}

// getResourcePage builds and makes request to em7 api and return data as a
// resource.
func (r *ResourceExtractor) getResourcePage(page int) (*Resource, error) {
	if r.Name == "" {
		return nil, errors.New("no name specified")
	}

	path := fmt.Sprintf("/api/%s?limit=%d&offset=%d&extended_fetch=1", r.Name, PAGESIZE, page*PAGESIZE)
	info.Printf("About to request %s", path)
	respc, errc := get(r.extractor, path, retry)
	if err := <-errc; err != nil {
		info.Printf("Error reported requesting %s %v", path, err)
		fmt.Println("get error")
		return nil, err
	}
	resp := <-respc
	defer resp.Body.Close()

	// decode response
	var res *Resource
	err := json.NewDecoder(resp.Body).Decode(&res)

	if err != nil {
		info.Printf("err reported decoding %v", resp.Body)
	}

	return res, err
}
