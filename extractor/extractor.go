package extractor

import (
	"crypto/tls"
	"net/http"
	"sync"
	"time"

	"github.com/ivpusic/grpool"
)

const (
	// how many records per page. used when getting nested data
	PAGESIZE  = 500
	QUEUESIZE = 50

	// how many retry attempts when calling EM7 API
	MAXRETRIES = 5
	retry      = 5

	// default TIMEOUT on em7 api requests
	TIMEOUT = 120 * time.Second
	// time to wait before each request
	PREREQUESTPAUSE = 0 //250 * time.Millisecond
	// time to wait after getting an error on a request before retry
	ERRORBACKOFFPAUSE = 0 //5   * time.Second
)

var totalRetries = 0
var totalFails = 0

// Em7Extractor implements the Extractor interface for grabbing data out of
// science logic
type Em7Extractor struct {
	User     string
	Pass     string
	URL      string
	Paths    map[string]interface{}
	PoolSize int // how many concurrent requests can be made

	count  int
	target Loader
	test   bool
	pool   *grpool.Pool
	client *http.Client
}

// WithLoader assigns loader for a given extractor
func (e *Em7Extractor) WithLoader(l Loader) *Em7Extractor {
	e.target = l
	return e
}

// Extract begins extraction process for science logic
func (e *Em7Extractor) Extract() {
	info.Println("Extraction beginning")
	if e.target == nil {
		info.Println("no targets")
		return
	}

	if e.PoolSize <= 0 {
		e.PoolSize = 10
		info.Printf("Request pool size was invalid.")
	}
	info.Println("Request pool size:", e.PoolSize)

	e.count = 0
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	e.client = &http.Client{Timeout: TIMEOUT, Transport: tr}
	e.pool = grpool.NewPool(e.PoolSize, QUEUESIZE)
	e.target.Load(e.fanout())
	info.Println("Requests made   :", e.count)
	info.Println("Retries  made   :", totalRetries)
	info.Println("Requests failed :", totalFails)
	info.Println("Extraction done")
}

func (e *Em7Extractor) fanout() <-chan Object {
	out := make(chan Object)

	go func() {
		wg := &sync.WaitGroup{}

		for resource, paths := range e.Paths {
			p, ok := paths.(map[string]interface{})
			if ok {
				wg.Add(1)
				go e.extract(resource, p, out, wg)
			} else {
				info.Printf("ignoring unexpected resource paths: %#v", paths)
			}
		}

		wg.Wait()
		close(out)
	}()

	return out
}

func (e *Em7Extractor) extract(name string, paths map[string]interface{}, out chan Object, wg *sync.WaitGroup) {
	defer wg.Done()
	re := &ResourceExtractor{name, paths, e}
	re.Extract(out)
	info.Println("finished extracting", name)
}

// get requests an http path from science logic with retry logic and thread
// pooling to throttle load on server.
func get(e *Em7Extractor, path string, retry int) (chan *http.Response, chan error) {
	e.count++

	out := make(chan *http.Response, 1)
	errc := make(chan error, 1)

	e.pool.JobQueue <- func() {
		defer close(out)
		defer close(errc)

		oc, ec := getRetry(e, path, retry)
		out <- oc
		errc <- ec
	}
	return out, errc
}

func getRetry(e *Em7Extractor, path string, retry int) (*http.Response, error) {
	req, err := http.NewRequest("GET", e.URL+path, nil)
	if err != nil {
		return nil, err
	}

	req.SetBasicAuth(e.User, e.Pass)

	//info.Println("sleeping for .2 seconds before pull")
	time.Sleep(PREREQUESTPAUSE)

	resp, err := e.client.Do(req)
	if err != nil || resp.StatusCode != 200 {
		if retry > 0 {
			totalRetries += 1
			info.Printf("retying, retries left %d: %s, StatusCode: %v", retry, path, resp.StatusCode)
			if resp.StatusCode != 200 {
				info.Printf("sleeping for %d seconds to back off the server", ERRORBACKOFFPAUSE)
				time.Sleep(ERRORBACKOFFPAUSE)
			}
			return getRetry(e, path, retry-1)
		} else {
			totalFails += 1
			debug.Printf("Retry failed for %s  -  %d tries remained", path, retry)
			return nil, err
		}
	}

	if retry < MAXRETRIES {
		info.Printf("Retry worked for %s  -  %d tries remained", path, retry)
	}

	return resp, nil
}
