package extractor

import (
	"sync"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const mgoPrefix = "em7_"

// Object provides a means of passing data around pipes
type Object struct {
	Name string
	Data []map[string]interface{}
}

// Loader provides a generic interface for loading data
type Loader interface {
	Load(<-chan Object)
}

// MongoLoader implements the loader interface for loading data into mongodb
type MongoLoader struct {
	URI string
	DB  string
}

// Load accepts Objects from the in channel and inserts data into mongo
func (m MongoLoader) Load(in <-chan Object) {
	session, err := mgo.Dial(m.URI)
	if err != nil {
		panic(err)
	}
	defer session.Close()

	wg := &sync.WaitGroup{}

	// insert objects recieved over the channel
	for o := range in {
		wg.Add(1)
		go m.insert(session, o, wg)
	}

	wg.Wait()
}

func (m MongoLoader) insert(session *mgo.Session, o Object, wg *sync.WaitGroup) {
	defer wg.Done()

	c := session.DB(m.DB).C(mgoPrefix + o.Name)
	for _, v := range o.Data {
		if err := c.Insert(v); err != nil {
			if id, ok := v["id"]; ok {
				info.Println("failed to insert", id)
			} else {
				info.Println("failed to insert", v)
			}
		}
	}
	info.Printf(" inserted %d %s records\n", len(o.Data), o.Name)
}

// Cleanup removes all data from mongodb for each root in paths
func Cleanup(uri, db string, paths map[string]interface{}) error {
	debug.Println("cleaning up mongo:", uri)
	session, err := mgo.Dial(uri)
	if err != nil {
		return err
	}
	defer session.Close()

	for k := range paths {
		c := session.DB(db).C(mgoPrefix + k)
		_, err := c.RemoveAll(bson.M{})
		if err != nil {
			return err
		}
	}
	return nil
}
