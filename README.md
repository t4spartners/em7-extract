# Science Logic Bulk Data Extractor

A configurable bulk data extractor for science logic.  This app gathers all SL device/asset/etc... information and puts it into Mongo.

## Configuration

Configuration can be provided through command line flags, environment viarables, json, yaml, or toml files, and etcd in the listed order of precedence.

The simplest way of setting up config is through a file named ".em7.json" in either the users $HOME folder or the same folder as the program is running from.


## Deamon

The application can be run as a console application that kicks off immediately, or as a deamon to accept GRPC calls to begin extraction.

## Building

After cloning the repo, you should be able to just run "go build" to generate an executable.
