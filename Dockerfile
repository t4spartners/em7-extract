# Build Container
FROM golang:alpine
RUN apk -U add make git
WORKDIR /go/src/gitlab.com/t4spartners/em7-extract
ADD . .
RUN make deps; make install

# Final Build Image
FROM alpine:latest

RUN apk add -U ca-certificates && \
    update-ca-certificates && \
    rm -rf /var/cache/apk/*
COPY --from=0 /go/bin/em7-extract /bin
ENTRYPOINT [ "/bin/em7-extract" ]

EXPOSE 50051
