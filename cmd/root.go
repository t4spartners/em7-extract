// Copyright © 2017 T4S Partners
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/t4spartners/em7-extract/extractor"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string
	version string

	info  = log.New(os.Stdout, "", log.LstdFlags)
	debug extractor.Logger
)

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "em7",
	Short: "Extract data from Science Logic",
	Run: func(cmd *cobra.Command, args []string) {
		if viper.GetBool("version") {
			info.Printf("Cherwell Service Management CLI %s\n", version)
			return
		}

		// initializer loggers
		extractor.SetLogger(info)
		if viper.GetBool("verbose") {
			debug = log.New(os.Stdout, "", log.Lshortfile)
			extractor.SetDebugLogger(debug)
		}

		// initialize paths
		paths := getPaths(viper.GetStringSlice("path_str"))
		if !viper.GetBool("deamon") {
			if len(paths) == 0 {
				info.Println("path_str nil. No paths to export")
				return
			}
		}

		e := &extractor.Em7Extractor{
			User:     viper.GetString("em7.user"),
			Pass:     viper.GetString("em7.pass"),
			URL:      viper.GetString("em7.url"),
			PoolSize: viper.GetInt("em7.poolsize"),
			Paths:    paths,
		}

		// mongo loader
		l := &extractor.MongoLoader{
			URI: viper.GetString("mongo.uri"),
			DB:  viper.GetString("mongo.db"),
		}
		e.WithLoader(l)

		if err := extractor.Cleanup(l.URI, l.DB, paths); err != nil {
			fmt.Fprintln(os.Stderr, "mongo  cleanup failed:", err)
			return
		}

		if viper.GetBool("deamon") {
			extractor.Listen(e, l, viper.GetString("port"))
		} else {
			e.Extract()
		}
	},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		info.Println(err)
		os.Exit(-1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file")

	RootCmd.PersistentFlags().BoolP("verbose", "v", false, "verbose output")
	viper.BindPFlag("verbose", RootCmd.PersistentFlags().Lookup("verbose"))

	RootCmd.PersistentFlags().BoolP("deamon", "d", false, "run extractor as a deamon")
	viper.BindPFlag("deamon", RootCmd.PersistentFlags().Lookup("deamon"))

	RootCmd.PersistentFlags().StringP("port", "p", "50051", "port for deamon to listen on")
	viper.BindPFlag("port", RootCmd.PersistentFlags().Lookup("port"))

	RootCmd.Flags().Bool("version", false, "print version")
	viper.BindPFlag("version", RootCmd.Flags().Lookup("version"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	loadDefaultSettings()

	// environment configration
	viper.SetEnvPrefix("EM7")
	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))

	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName(".em7")
	viper.AddConfigPath("$HOME")
	viper.AddConfigPath(".")
	viper.AddConfigPath("/etc/cherwell-integration")
	viper.AutomaticEnv()
	if err := viper.ReadInConfig(); err == nil {
		if viper.GetBool("verbose") {
			info.Println("Using config file:", viper.ConfigFileUsed())
		}
	}
}

func loadDefaultSettings() {
	// mongo defaults
	viper.SetDefault("mongo.uri", "localhost")
	viper.SetDefault("mongo.db", "dvr")

	// concurrency defaults
	viper.SetDefault("em7.poolsize", 200)
	viper.SetDefault("page_size", 500)
	viper.SetDefault("workers", 500)
	viper.SetDefault("queue_size", 100)
	viper.RegisterAlias("PageSize", "page_size")
	viper.RegisterAlias("QueueSize", "queue_size")
}

func getPaths(pathStr []string) map[string]interface{} {
	objects := make(map[string]interface{})
	for _, o := range pathStr {
		path := strings.Split(o, ".")
		add(objects, path)
	}
	return objects
}

func add(paths map[string]interface{}, path []string) {
	if len(path) == 0 {
		return
	}

	next, ok := paths[path[0]]
	if !ok {
		paths[path[0]] = make(map[string]interface{})
		next = paths[path[0]]
	}
	add(next.(map[string]interface{}), path[1:])
}
